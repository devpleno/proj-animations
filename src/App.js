import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'

class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      items: [1, 2, 3]
    }

    this.remove = this.remove.bind(this)
  }

  remove(index) {
    let newsItems = [...this.state.items]
    newsItems.splice(index, 1)
    this.setState({ items: newsItems })
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>

        <ReactCSSTransitionGroup transitionName="anim" transitionLeaveTimeout={500} transitionEnterTimeout={500}>
          {this.state.items.map((value, i) => {
            return (
              <div key={value}>
                <span> {value} </span>
                <button onClick={() => this.remove(i)}>x</button>
              </div>
            )
          })
          }
        </ReactCSSTransitionGroup>

        <button onClick={() => this.setState({ items: [...this.state.items, new Date().getTime()] })}>Clique aqui</button>
      </div>
    );
  }
}

export default App;
